import axios from "axios";

export const ADD_STUDENT = "ADD_STUDENT";
export const INCREASE_HOUSE_POINTS = "INCREASE_HOUSE_POINTS";
export const DECREASE_HOUSE_POINTS = "DECREASE_HOUSE_POINTS";

export interface housePointsInterface {
  type: typeof DECREASE_HOUSE_POINTS | typeof INCREASE_HOUSE_POINTS;
  house: {
    name: string;
    points: number;
  };
}

export interface addStudentInterface {
  type: typeof ADD_STUDENT;
  student: Object;
}

interface dispatchParams {
  name: string;
  house: string;
  image: string;
}

const addStudent = (student: object): addStudentInterface => ({
  type: ADD_STUDENT,
  student,
});

export const toAddStudent = () => (dispatch: any) => {
  axios
    .get("http://hp-api.herokuapp.com/api/characters/students")
    .then((res) =>
      res.data.map(({ name, house, image }: dispatchParams) =>
        dispatch(addStudent({ name, house, image }))
      )
    );
};

export const decreaseHousePoints = (house: {
  name: string;
  points: number;
}): housePointsInterface => ({
  type: DECREASE_HOUSE_POINTS,
  house: house,
});

export const increaseHousePoints = (house: {
  name: string;
  points: number;
}): housePointsInterface => ({
  type: INCREASE_HOUSE_POINTS,
  house: house,
});

import { ADD_STUDENT } from "../actions";

const defaultState: Array<object> = [];

interface Actions {
  type: String;
  student: Object;
}

const students = (state = defaultState, action: Actions) => {
  switch (action.type) {
    case ADD_STUDENT:
      return [...state, action.student];
    default:
      return state;
  }
};

export default students;

import { combineReducers } from "redux";
import students from "./students";
import houses from "./houses";

export interface reducerInterface {
  houses: Array<{
    name: string;
    points: number;
    image: string;
  }>;
  students: Array<{
    name: string;
    house: string;
    image: string;
  }>;
}

export default combineReducers({ students, houses });

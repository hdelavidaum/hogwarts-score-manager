import {
  DECREASE_HOUSE_POINTS,
  INCREASE_HOUSE_POINTS,
  housePointsInterface,
} from "../actions";

import {
  GryffindorShield,
  HufflePuffShield,
  RavenclawShield,
  SlytherinShield,
} from "../../images";

const defaultState = [
  {
    name: "Gryffindor",
    points: 500,
    image: GryffindorShield,
  },
  {
    name: "Hufflepuff",
    points: 500,
    image: HufflePuffShield,
  },
  {
    name: "Ravenclaw",
    points: 500,
    image: RavenclawShield,
  },
  {
    name: "Slytherin",
    points: 500,
    image: SlytherinShield,
  },
];

const housesReducer = (
  state = defaultState,
  { type, house }: housePointsInterface
) => {
  switch (type) {
    case DECREASE_HOUSE_POINTS:
      return [
        ...state.map((item) => {
          if (item.name === house.name) {
            return { ...item, points: item.points - house.points };
          }
          return item;
        }),
      ];
    case INCREASE_HOUSE_POINTS:
      return [
        ...state.map((item) => {
          if (item.name === house.name) {
            return { ...item, points: item.points + house.points };
          }
          return item;
        }),
      ];
    default:
      return state;
  }
};

export default housesReducer;

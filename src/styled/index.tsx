import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  margin: 0;
  padding: 0;
  width: 100vw;
  height: 100vh;
  background: #e5e5e5;
  overflow: auto;
  display: flex;
  flex-flow: column nowrap;
  justify-content: flex-start;
  align-items: center;
  align-content: center;

  &:after {
    content: "";
    height: 280px;
    width: 100%;
    z-index: 0;
    background: #6d9fea;
    position: absolute;
  }
`;

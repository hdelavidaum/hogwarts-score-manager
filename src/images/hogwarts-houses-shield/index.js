import GryffindorShield from "./gryffindor-shield.svg";
import HufflePuffShield from "./hufflepuff-shield.svg";
import RavenclawShield from "./ravenclow-shield.svg";
import SlytherinShield from "./slytherin-shield.svg";

import HogwartsShield from "./hogwarts-shield.svg";

export {
  HogwartsShield,
  GryffindorShield,
  HufflePuffShield,
  RavenclawShield,
  SlytherinShield,
};

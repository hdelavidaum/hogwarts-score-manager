import styled from "styled-components";

export const Wrapper = styled.div`
  width: 90%;
`;

export const Items = styled.div`
  width: 100%;
  padding: 10px 0;
  border-bottom: 1px solid #5777a8;
  color: #5777a8;
  font-size: 20px;
  font-weight: 200;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
`;

export const ItemsTitle = styled(Items)`
  font-weight: bold;
`;

export const Name = styled.div`
  width: 60%;
`;

export const House = styled.div`
  width: 30%;
`;

export const StyledScorer = styled.img`
  width: 10%;
  height: 28px;
`;

import React, { useState } from "react";
import { Wrapper, Items, Name, House, StyledScorer } from "./styled";
import { ScorerModal } from "../index";

interface Props {
  name: string;
  image: string;
  house: string;
  scorer?: string;
}

const Student = ({ name, house, scorer, image }: Props) => {
  const [isVisible, setVisibility] = useState(false);

  return (
    <>
      <Wrapper>
        <Items>
          <Name>{name}</Name>
          <House>{house}</House>
          <StyledScorer
            src={scorer}
            onClick={() => setVisibility(!isVisible)}
          />
        </Items>
      </Wrapper>
      <ScorerModal
        name={name}
        house={house}
        image={image}
        visibility={isVisible}
        setVisibility={setVisibility}
      />
    </>
  );
};

export default Student;

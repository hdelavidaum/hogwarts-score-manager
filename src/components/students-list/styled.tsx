import styled from "styled-components";

export const Wrapper = styled.div`
  width: calc(100% - 60px);
  margin-bottom: 60px;
  padding: 15px 0 50px 0;
  height: fit-content;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
  background-color: #e5edf2;
`;

export const Title = styled.div`
  width: 90%;
  margin: 25px 0;
  font-size: 35px;
  font-weight: 300;
  color: #5777a8;
`;

export const ItemsTitle = styled.div`
  width: 90%;
  padding: 10px 0;
  border-bottom: 1px solid #5777a8;
  color: #5777a8;
  font-size: 20px;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  font-weight: bold;
`;

export const Name = styled.div`
  width: 60%;
`;

export const House = styled.div`
  width: 30%;
`;

import React from "react";
import { Wrapper, Title, ItemsTitle, Name, House } from "./styled";
import { useSelector } from "react-redux";
import { reducerInterface } from "../../redux/reducers";
import { Student } from "../";
import { Scorer } from "../../images";

const StudentsList = () => {
  const students = useSelector(({ students }: reducerInterface) =>
    students.sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    })
  );

  return (
    <Wrapper>
      <Title>Alunos</Title>
      <ItemsTitle>
        <Name>Name</Name>
        <House>House</House>
      </ItemsTitle>
      {students &&
        students.map(({ name, house, image }, key) => {
          return (
            <Student
              name={name}
              house={house}
              scorer={Scorer}
              image={image}
              key={key}
            />
          );
        })}
    </Wrapper>
  );
};

export default StudentsList;

import Title from "./title";
import HousePoints from "./house-points";
import HousesWrapper from "./houses-wrapper";
import Student from "./student";
import StudentsList from "./students-list";
import ScorerModal from "./scorer-modal";

export {
  Title,
  HousePoints,
  HousesWrapper,
  StudentsList,
  Student,
  ScorerModal,
};

import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { increaseHousePoints, decreaseHousePoints } from "../../redux/actions";
import { IoMdClose } from "react-icons/io";
import {
  Wrapper,
  Modal,
  Closer,
  StudentAvatar,
  Content,
  TitleWrapper,
  HouseShield,
  HouseName,
  PointsSetter,
  GainButton,
  LoseButton,
  InputScore,
  StudentName,
  PointsToGain,
  PointsToLose,
  DoneButton,
} from "./styled";
import {
  GryffindorShield,
  HufflePuffShield,
  RavenclawShield,
  SlytherinShield,
} from "../../images";

interface Props {
  name: string;
  house: string;
  image: string;
  visibility: boolean;
  setVisibility: React.Dispatch<React.SetStateAction<boolean>>;
}

const defineHouseShield = (house: string) => {
  if (house === "Gryffindor") return GryffindorShield;
  if (house === "Hufflepuff") return HufflePuffShield;
  if (house === "Ravenclaw") return RavenclawShield;
  if (house === "Slytherin") return SlytherinShield;
};

const ScorerModal = ({
  name,
  house,
  image,
  visibility,
  setVisibility,
}: Props) => {
  const dispatch = useDispatch();
  const [points, setPoints] = useState(0);
  const [hasPointsSetted, setHasPoints] = useState(false);
  const [isToGainPoints, setToGainPoints] = useState(true);

  const handleOnClick = () => {
    setVisibility((prevState) => !prevState);
    setPoints(0);
    setHasPoints(!hasPointsSetted);
    setToGainPoints(!isToGainPoints);
  };

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (Number(e.target.value) < 0) return null;
    setPoints(Number(e.target.value));
  };

  const handleGainPoints = () => {
    setHasPoints(!hasPointsSetted);
    setToGainPoints(true);
  };

  const handleLosePoints = () => {
    setHasPoints(!hasPointsSetted);
    setToGainPoints(false);
  };

  const handleDispatchPoints = () => {
    isToGainPoints
      ? dispatch(increaseHousePoints({ name: house, points: points }))
      : dispatch(decreaseHousePoints({ name: house, points: points }));

    setPoints(0);
    setHasPoints(false);
    setVisibility((prevState) => !prevState);
  };

  return (
    <Wrapper visible={visibility}>
      <Modal>
        <Closer onClick={handleOnClick}>
          <IoMdClose />
        </Closer>
        <StudentAvatar src={image} alt={""} />
        <Content>
          <TitleWrapper>
            <HouseShield src={defineHouseShield(house)} alt={""} />
            <HouseName>{house}</HouseName>
          </TitleWrapper>
          <StudentName>{name}</StudentName>
          {hasPointsSetted ? (
            <>
              {isToGainPoints ? (
                <PointsToGain>{`+${points}`}</PointsToGain>
              ) : (
                <PointsToLose>{`-${points}`}</PointsToLose>
              )}
              <DoneButton onClick={handleDispatchPoints}>Done</DoneButton>
            </>
          ) : (
            <PointsSetter>
              <InputScore
                type="number"
                placeholder="Insert points amount"
                value={points}
                onChange={handleOnChange}
              />
              <GainButton onClick={handleGainPoints}> Gain </GainButton>
              <LoseButton onClick={handleLosePoints}> Lose</LoseButton>
            </PointsSetter>
          )}
        </Content>
      </Modal>
    </Wrapper>
  );
};

export default ScorerModal;

import styled from "styled-components";

interface Props {
  image?: string;
  shield?: string;
  visible?: boolean;
}

export const Wrapper = styled.div`
  position: fixed;
  display: flex;
  flex-flow: row nowrap;
  place-content: center center;
  align-items: center;
  top: 0;
  height: 100%;
  width: 100%;
  background-color: rgba(23, 23, 23, 0.5);
  z-index: 2;
  visibility: ${({ visible }: Props) => (visible ? "visible" : "hidden")};
`;

export const Modal = styled.div`
  width: 770px;
  height: 400px;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-around;
  align-content: center;
  align-items: center;
  z-index: 5;
  color: #5777a8;
  background-color: #e5edf2;
  position: relative;
`;

export const Closer = styled.div`
  position: absolute;
  top: 15px;
  right: 15px;
  height: 30px;
  width: 30px;
  font-size: 30px;
  color: #5777a8;
`;

export const StudentAvatar = styled.img`
  height: 90%;
  width: auto;
  max-width: 55%;
`;

export const Content = styled.div`
  height: 90%;
  width: 60%;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
  align-items: center;
  align-content: center;
`;
export const TitleWrapper = styled.div`
  height: 20%;
  width: 85%;
  align-self: flex-start;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
  align-content: center;
`;

export const HouseShield = styled.img`
  height: 100%;
  width: auto;
`;

export const HouseName = styled.div`
  padding: 0 20px;
  height: fit-content;
  width: fit-content;
  font-size: 40px;
  font-weight: 300;
`;

export const StudentName = styled.div`
  font-size: 45px;
  height: 25%;
  width: fit-content;
  font-weight: 400;
`;

export const PointsSetter = styled.div`
  width: 60%;
  height: fit-content;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-content: center;
  align-items: center;
`;

export const GainButton = styled.button`
  height: 40px;
  width: 40%;
  font-size: 25px;
  color: #fff;
  background-color: #65e1cb;
  border: none;
  border-radius: 3px;

  &:hover {
    box-shadow: 0 0 3px #333;
  }
`;

export const LoseButton = styled(GainButton)`
  background-color: #f8a388;
`;

export const InputScore = styled.input`
  margin: 20px 0;
  width: 100%;
  height: 40px;
  font-size: 21px;
  text-align: center;

  &:focus {
    box-shadow: inset 0 0 1px rgba(52, 97, 164, 0.8);
  }
`;

export const PointsToGain = styled.div`
  text-align: center;
  width: 100%;
  height: fit-content;
  font-size: 60px;
  color: #65e1cb;
`;

export const PointsToLose = styled(PointsToGain)`
  color: #f8a388;
`;

export const DoneButton = styled(GainButton)`
  background-color: #6994d4;
`;

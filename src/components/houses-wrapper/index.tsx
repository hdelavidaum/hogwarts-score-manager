import React from "react";
import { Wrapper } from "./styled";
import HousePoints from "../house-points";
import { useSelector } from "react-redux";
import { reducerInterface } from "../../redux/reducers";

const HousesWrapper = () => {
  const houses = useSelector(({ houses }: reducerInterface) =>
    houses.sort((a, b) => {
      if (a.points < b.points) return 1;
      if (a.points > b.points) return -1;
      return 0;
    })
  );

  return (
    <Wrapper>
      {houses.map((house, key) => {
        return (
          <HousePoints
            house={house.name}
            image={house.image}
            points={house.points}
            position={key + 1}
            key={key}
          />
        );
      })}
    </Wrapper>
  );
};

export default HousesWrapper;

import styled from "styled-components";

export const Wrapper = styled.div`
  margin: 20px 0;
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
  align-content: center;
  z-index: 1;
`;

import React from "react";
import { Content, Logo, HeaderH1 } from "./styled";
import { HogwartsShield } from "../../images";

const Title = () => {
  return (
    <Content>
      <Logo src={HogwartsShield} alt="" />
      <HeaderH1>Hogwarts Score Manager</HeaderH1>
    </Content>
  );
};

export default Title;

import styled from "styled-components";

export const Content = styled.div`
  top: 0;
  margin-top: 50px;
  width: 100%;
  height: 125px;
  background-color: #3461a4;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  align-content: center;
  align-items: center;
  z-index: 1;
`;

export const Logo = styled.img`
  height: 85%;
  margin: 0 50px;
`;

export const HeaderH1 = styled.h1`
  font-weight: 400;
  font-size: 38px;
  color: #ffffff;
`;

import React from "react";
import { Wrapper, HouseShield, StyledH2, StyledP } from "./styled";

interface Params {
  image: string;
  house: string;
  points: number;
  position: string | number;
}

const HousePoints = ({ image, house, points, position }: Params) => {
  return (
    <Wrapper>
      <StyledH2>{`#${position} ${house}`}</StyledH2>
      <HouseShield src={image} alt="" />
      <StyledP>{points}</StyledP>
    </Wrapper>
  );
};

export default HousePoints;

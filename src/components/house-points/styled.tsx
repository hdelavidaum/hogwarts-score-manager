import styled from "styled-components";

export const Wrapper = styled.div`
  margin: 30px;
  height: 500px;
  width: 300px;
  background: #e5edf2;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  align-content: center;
  align-items: center;
`;
export const StyledH2 = styled.h2`
  font-size: 30px;
  font-weight: 400;
  color: #5777a8;
`;
export const StyledP = styled.p`
  font-size: 30px;
  font-weight: 400;
  color: #5777a8;
`;
export const HouseShield = styled.img`
  color: #5777a8;
  height: auto;
  width: 85%;
`;

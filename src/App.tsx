import React, { useEffect } from "react";
import "./App.css";
import { useDispatch } from "react-redux";
import { toAddStudent } from "./redux/actions";

import { Title, HousesWrapper, StudentsList } from "./components";
import { Container } from "./styled";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(toAddStudent());
  }, [dispatch]);

  return (
    <Container>
      <Title />
      <HousesWrapper />
      <StudentsList />
    </Container>
  );
};

export default App;
